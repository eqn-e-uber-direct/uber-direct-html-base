$(document).ready(function () {
	$('.eye').click(function () {
		$('body').toggleClass('eyesOff');

		const bodyClass = $('body').hasClass('eyesOff');
		const passwordInput = $('#passwordLogin');

		if (bodyClass) {
			passwordInput.attr('type', 'text');
		} else {
			passwordInput.attr('type', 'password');
		}
	});

	// Adicionei uma classe ao input com classe wrongInfo (para simular o input com erro) e adicionar o css de erro somente nesse input
	$('.enter').click(function (event) {
		event.preventDefault();
		$('.wrongInfo').addClass('loginError');
		$('.wrong-field').addClass('active');
	});

	// GROUP-DETAIL
	var input = $('.order-amount input');
	var lessButton = $('.order-amount .less');
	var moreButton = $('.order-amount .more');

	lessButton.on('click', function () {
		var value = parseInt(input.val()) || 0; // Garante que o valor seja sempre um número ou zero
		if (value > 0) {
			value--;
			input.val(value);
			if (value === 0) {
				lessButton.addClass('disabled');
			}
		}
	});

	moreButton.on('click', function () {
		var value = parseInt(input.val()) || 0; // Garante que o valor seja sempre um número ou zero
		value++;
		input.val(value);
		lessButton.removeClass('disabled');
	});

	// GROUP-LOCAL

	$('.tabs h3').click(function () {
		if (!$(this).hasClass('active')) {
			$('body').toggleClass('delivery');
			// Adiciona a classe "active" à tab clicada
			$(this).addClass('active');
			// Remove a classe "active" de todas as outras tabs dentro de .tabs
			$('.tabs h3').not(this).removeClass('active');
		}
	});

	$('.up').click(function (e) {
		e.preventDefault();

		var target = $(this.hash);

		$('html').animate(
			{
				scrollTop: target.offset().top - 120,
			},
			800
		);
	});

	var button = $('.up');

	function checkScroll() {
		var screenHeight = $(window).height();

		if ($(window).scrollTop() > screenHeight) {
			button.addClass('active');
		} else {
			button.removeClass('active');
		}
	}

	// Adicionar um evento de rolagem à janela
	$(window).scroll(checkScroll);
});
